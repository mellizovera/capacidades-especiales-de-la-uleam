const express = require('express');
const cors = require('cors');

const app = express();
PORT = process.env.PORT || 2000;
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cors());
app.use(require('../src/routes/estudiantes/estudiante'));
app.use(require('../src/routes/representantes/representante'));
app.use(require('../src/routes/tutores/tutor'));
app.use(require('../src/routes/segumientos-medicos/tratamientos-medicos/consulta'));
app.use(require('../src/routes/segumientos-medicos/tratamientos-medicos/cumplido'));
app.use(require('../src/routes/segumientos-medicos/tratamientos-medicos/nocumplido'));
app.use(require('../src/routes/segumientos-academicos/asignaturas/asignatura'));
app.use(require('../src/routes/segumientos-academicos/asignaturas/aprobada'));
app.use(require('../src/routes/segumientos-academicos/asignaturas/reprobada'));
app.use(require('../src/routes/historial-medico/medico'));
app.use(require('../src/routes/historial-academico/academico'));


app.listen(PORT);
console.log('server corriendo por el puerto',PORT);

