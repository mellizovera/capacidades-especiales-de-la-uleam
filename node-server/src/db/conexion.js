const { Pool } = require('pg');


const pool = new Pool({
    user: process.env.user || 'postgres',
    host: process.env.host || 'localhost',
    database: process.env.database || 'capacidades_especiales',
    password: process.env.password || '19982012',
    port: process.env.port || '5432'
});

module.exports = {
    pool
}